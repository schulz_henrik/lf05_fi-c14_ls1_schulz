
public class Ausgabe {

	public static void main(String[] args) {
		//print() + println()
		
		System.out.print("Der Planet Namek explodiert in 5 Minuten.");
		System.out.println(" Dies dauert 20 Episoden und Krillin stirbt.");
		
		//println() + println()
		
		System.out.println("Der Planet Namek explodiert in 5 Minuten.");
		System.out.println("Dies dauert 20 Episoden und Krillin stirbt.");
		
		//print() + print() mit Zeilenumschwung \n
		
		System.out.print("Der Planet Namek explodiert in 5 Minuten.\n");
		System.out.print("Dies dauert 20 Episoden und Krillin stirbt.\n");
		
		//Anführungszeichen
		
		System.out.print("Der Planet Namek explodiert in \"5 Minuten\".");
		System.out.println(" Dies dauert 20 Episoden und Krillin stirbt.");
		
		//String + Zahlen
		
		int x= 5;
		int y= 20;
		
		System.out.println("Der Planet Namek explodiert in " + x + " Minuten.");
		System.out.println("Dies dauert " + y +  " Episoden und Krillin stirbt.");
		
			//print() schreibt ohne Zeilenumbrüche. println() schreibt jeweils nur in eine zeile, d.h. es besitzt einen verstecketen Zeilenumbruch.
		
		//eine kleine Raute
		
		String s = "*";
		
		System.out.printf("%14s\n", s);
		System.out.printf("%15s\n", s + s + s);
		System.out.printf("%16s\n", s + s + s + s + s);
		System.out.printf("%17s\n", s + s + s + s + s + s + s);
		System.out.printf("%16s\n", s + s + s + s + s);
		System.out.printf("%15s\n", s + s + s);
		System.out.printf("%14s\n", s);

		//auf die 2. dezimalstelle kürzen

		double Z1 = 22.4234234;
		double Z2 = 111.2222;
		double Z3 = 4.0;
		double Z4 = 1000000.551;
		double Z5 = 97.34;
		
		System.out.printf("%.2f\n" , Z1);
		System.out.printf("%.2f\n" , Z2);
		System.out.printf("%.2f\n" , Z3);
		System.out.printf("%.2f\n" , Z4);
		System.out.printf("%.2f\n" , Z5);



	}

}
