import java.util.Scanner;

public class Person {

	public static void main(String[] args) {
		
		//Eingabe deklarieren
		Scanner myScanner = new Scanner(System.in);
		
		//Eingebae des Namen
		System.out.print("Bitte geben Sie Ihren Namen ein: ");
		
		//Namen Speichern
		String name = myScanner.next();
		
		//Eingabe des Vornamen
		System.out.print("Bitte geben Sie Ihren Vornamen ein: ");

		//Vorname speichern
		String vorname = myScanner.next();
		
		//Eingabe des Alters
		System.out.print("Bitte geben Sie Ihr Alter ein: ");
		
		//Alter Speichern
		int alter = myScanner.nextInt();
		
		//Personandaten ausgeben
		System.out.println("Ihr Name lautet" + " " + name + "," + " " + vorname + ".");
		System.out.println("Sie sind" + " " + alter + " " + "Jahre alt.");

		

	}

}
