
public class Aufgabe2 {

	public static void main(String[] args) {

	//Aufgabe 2
	
		//Anfang String
			String a1 = "0!\t=";
			String a2 = "1!\t=";
			String a3 = "2!\t=";
			String a4 = "3!\t=";
			String a5 = "4!\t=";
			String a6 = "5!\t=";
	
		//Mitte String
			String m1 = " ";
			String m2 = "1";
			String m3 = "1 * 2";
			String m4 = "1 * 2 * 3";
			String m5 = "1 * 2 * 3 * 4";
			String m6 = "1 * 2 * 3 * 4 * 5";

		//Ende String
			String e1 = "1";
			String e2 = "1";
			String e3 = "2";
			String e4 = "6";
			String e5 = "24";
			String e6 = "120";
			String g = "=";
	
	
		//Zeile 1
			System.out.printf("%-5s", a1);
			System.out.printf("%-19s", m1);
			System.out.printf(g + "%4s\n", e1);

		//Zeile 2
			System.out.printf("%-5s", a2);
			System.out.printf("%-19s", m2);
			System.out.printf(g + "%4s\n", e2);

		//Zeile 3
			System.out.printf("%-5s", a3);
			System.out.printf("%-19s", m3);
			System.out.printf(g + "%4s\n", e3);

		//Zeile 4
			System.out.printf("%-5s", a4);
			System.out.printf("%-19s", m4);
			System.out.printf(g + "%4s\n", e4);

		//Zeile 5
			System.out.printf("%-5s", a5);
			System.out.printf("%-19s", m5);
			System.out.printf(g + "%4s\n", e5);

		//Zeile 6
			System.out.printf("%-5s", a6);
			System.out.printf("%-19s", m6);
			System.out.printf(g + "%4s\n", e6);
	} 	
}
