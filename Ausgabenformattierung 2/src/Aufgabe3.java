
public class Aufgabe3 {

	public static void main(String[] args) {
		
		//Variabeln definieren
		String b = "|";
		String fahren = "Fahrenheit";
		String cel = "Celcius";
		String z = "------------------------";
		String z2 = "+";
		String z3 = "-";
		
		int f1 = 20;
		int f2 = 10;
		int f3 = 0;
		int f4 = 20;
		int f5 = 30;
		
		double c1 =	28.89;
		double c2 = 23.33;
		double c3 = 17.78;
		double c4 = 6.67;
		double c5 = 1.11;
		
		//Kopfzeile
		System.out.printf("%-12s", fahren);
		System.out.printf("%2s", b);
		System.out.printf("%10s\n", cel);
		
		//trennlinie
		System.out.println(z);
		
		//zeile 1
		System.out.printf("%-12s", z3 + f1);
		System.out.printf("%2s", b);
		System.out.printf("%10s\n", z3 + c1);
		
		//zeile 2
		System.out.printf("%-12s", z3 + f2);
		System.out.printf("%2s", b);
		System.out.printf("%10s\n", z3 + c2);
		
		//zeile 3
		System.out.printf("%-12s", z2 + f3);
		System.out.printf("%2s", b);
		System.out.printf("%10s\n", z3 + c3);
		
		//zeile 4
		System.out.printf("%-12s", z2 + f4);
		System.out.printf("%2s", b);
		System.out.printf("%10s\n", z3 + c4);

		//zeile 5
		System.out.printf("%-12s", z2 + f5);
		System.out.printf("%2s", b);
		System.out.printf("%10s\n", z3 + c5);


	}

}
